import { useParams, Link } from 'react-router-dom';
import { useEffect, useState, Fragment } from 'react';
import { Container, Row, Col, Table, Button, Form, Spinner } from 'react-bootstrap';

import toastr from 'toastr';
import BootstrapSwitchButton from 'bootstrap-switch-button-react';

export default function Profile(){

	const {userId} = useParams();

	const admin = localStorage.getItem('admin');
	const token = localStorage.getItem('token');

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [isAdmin, setIsAdmin] = useState(false);
	const [mobileNo, setMobileNo] = useState("");
	const [isActive, setIsActive] = useState(true);
	const [myId, setMyId] = useState("")
	const [pwVisibility, setPwVisbility] = useState(false);
	const [newPassword, setNewPassword] = useState("");
	const [verifyNewPassword, setVerifyNewPassword] = useState("");
	const [proceedDisable, setProceedDisable] = useState(true);
	const [pwVisibilityBtn, setPwVisibilityBtn] = useState(false);

	const [isProcessing, setIsProcessing] = useState(false);

	//from Registration, validation of email
	const [isValid, setIsValid] = useState(true);
	const [isLoading, setIsLoading] = useState("noLoad");

	useEffect(()=>{
		fetchData();
	},[])

	const fetchData = () => {

		let fetchAPI = "";

		if (userId !== undefined && admin === "true"){
			fetchAPI = `https://capstone2ecommerceapi.herokuapp.com/api/users/profile/${userId}`
		} else {
			fetchAPI = `https://capstone2ecommerceapi.herokuapp.com/api/users/my-profile`
		}

		setIsProcessing(true);

		fetch(fetchAPI, {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			if (result.message !== null){
				setFirstName(result.firstName);
				setLastName(result.lastName);
				setEmail(result.email);
				setIsAdmin(result.isAdmin);
				setMobileNo(result.mobileNo.toString());
				setIsActive(result.isActive);
				setMyId(result._id);
			} else {
				toastr.error(result.message);
				// alert(result.message);
			}

			setIsProcessing(false);
		})
	}

	const getBackground = () => {
		if (admin === "true"){
			return `adminPage my-auto`;
		} else {
			return `welcome my-auto`;
		}
	}

	const getMargin = () => {
		if (admin === "true"){
			return `my-auto mx-auto`;
		} else {
			return `my-auto ml-5`;
		}
	}

	const getColumn = () => {
		if (admin === "true"){
			return 6;
		} else {
			return 4;
		}
	}

	const updateAdminStatus = (userId) => {

		setIsProcessing(true);

		fetch(`https://capstone2ecommerceapi.herokuapp.com/api/users/change-user-status/${userId}`, {
			method: "PATCH",
			headers: { "Authorization": `Bearer ${token}` }
		})
		.then(result => result.json())
		.then(result => {
			if (result.message !== null){
				toastr.success(`Administrator status updated successfully.`)
				// alert(`Administrator status updated successfully.`);
				fetchData();
			} else {
				toastr.error(result.message)
				// alert(result.message);
			}

			setIsProcessing(false);
		})
	}

	const updateActiveStatus = (userId) => {

		setIsProcessing(true);

		fetch(`https://capstone2ecommerceapi.herokuapp.com/api/users/user-access/${userId}`, {
			method: "PATCH",
			headers: { "Authorization": `Bearer ${token}` }
		})
		.then(result => result.json())
		.then(result => {
			if (result.message !== null){
				toastr.success(`Active status updated successfully.`)
				// alert(`Active status updated successfully.`);
				fetchData();
			} else {
				toastr.error(result.message)
				// alert(result.message);
			}

			setIsProcessing(false);

		})
	}

	const validateEmailCSS = () => {
		if(isValid){
			return "login";
		} else {
			return "login loginFailed";
		}
	}

	const validateEmail = (emailAddress) => {
		
		setIsLoading("load");
		
		fetch('https://capstone2ecommerceapi.herokuapp.com/api/users/email-exists', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then(response => {
			if (response){
				setIsValid(false);
				toastr.warning('Email already registered.');
				// alert('Email already registered.');
			} else {
				setIsValid(true);
			}
			setIsLoading("noLoad");
		})
	}

	const displayLoading = () => {
		
		if (isLoading === "load"){
			return "spinner-border spinner-border-sm mx-1";
		} else if (isLoading === "noLoad"){
			return null;
		}
	}

	const updateProfile = (e) => {
		e.preventDefault();

		setIsProcessing(true);

		fetch(`https://capstone2ecommerceapi.herokuapp.com/api/users/update/${myId}`, {
			method: "PUT",
			headers: {
				"Authorization": `Bearer ${token}`,
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo
			})
		})
		.then(result => result.json())
		.then(result => {
			if(result.message !== null){
				toastr.success(`User details successfully updated.`);
				// alert(`User details successfully updated.`);
			} else {
				toastr.warning(result.message);
				// alert(result.message);
			}

			setIsProcessing(false);
		})

	}

	const changeVisibility = () => {
		setPwVisbility(true);
		setPwVisibilityBtn(true);
	}

	const changePassword = (e) => {
		e.preventDefault();

		setIsProcessing(true);

		// fetch(`http://localhost:3008/api/users/update-user-password/${myId}`, {
		fetch(`https://capstone2ecommerceapi.herokuapp.com/api/users/update-user-password/${myId}`, {
			method: "PATCH",
			headers: {
				"Authorization": `Bearer ${token}`,
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				password: newPassword
			})
		})
		.then(result => result.json())
		.then(result => {
			
			if (result === true){
				toastr.success(`Password changed.`);
				setPwVisbility(false);
				setNewPassword("");
				setVerifyNewPassword("");
				setProceedDisable(true);
			} else {
				toastr.error(result.message)
			}

			setIsProcessing(false);
			setPwVisibilityBtn(false)
		})

	}

	const passwordMatch = () =>{
		if (newPassword === verifyNewPassword){
			toastr.success(`Password matches!`)
			setProceedDisable(false);
		} else {
			toastr.warning(`Passwords do not match.`);
			setProceedDisable(true);
		}
	} 

	const getDisplay = () => {
		
		if (userId !== undefined){
			return(
			<Fragment>
				<Table className="tableBackground mt-4 fadeInElements">
					<thead className="tableHeader">
					     <tr>
							<th></th>
							<th></th>
					    </tr>
					</thead>
					<tbody>
						<tr>
							<td><strong>First Name:</strong></td>
							<td>{firstName}</td>
						</tr>
						<tr>
							<td><strong>Last Name:</strong></td>
							<td>{lastName}</td>
						</tr>
						<tr>
							<td><strong>Email Address:</strong></td>
							<td>{email}</td>
						</tr>
						<tr>
							<td><strong>Mobile Number:</strong></td>
							<td>{mobileNo}</td>
						</tr>
						<tr>
							<td><strong>Administrator:</strong></td>
							<td>
								<BootstrapSwitchButton
									checked={isAdmin}
									onlabel="Admin"
									offlabel="User"
									width={100}
									onChange={() => updateAdminStatus(userId)}
								/>
							</td>
						</tr>
						<tr>
							<td><strong>Active:</strong></td>
							<td>
								<BootstrapSwitchButton
									checked={isActive}
									onlabel="Active"
									offlabel="Inactive"
									width={100}
									onChange={() => updateActiveStatus(userId)}
								/>
							</td>
						</tr>
						<tr className="tableHeader">
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</Table>
				<Link to={`../all-users`} className="btn tableBtn mt-3">Back to list</Link>
			</Fragment>
			)
		} else {
			return(
			<Fragment>
				<Form onSubmit={(e) => updateProfile(e)} className="mt-3 fadeInElements">

					<Form.Group className="mb-3">
							<Form.Control 
								type="text" 
								className="login" 
								value={firstName}
								onChange={(e)=> setFirstName(e.target.value)}
							/>
					</Form.Group>

					<Form.Group className="mb-3">
							<Form.Control 
								type="text" 
								className="login" 
								value={lastName}
								onChange={(e)=> setLastName(e.target.value)}
							/>
					</Form.Group>

					<Form.Group className="mb-3">
						<Form.Control 
							type="email" 
							className={validateEmailCSS()}
							value={email}
							onChange={(e)=> setEmail(e.target.value)}
							onBlur={(e)=>validateEmail({email})}
						/>
						<span className={displayLoading()}></span>
					</Form.Group>

					<Form.Group className="mb-3">
						<Form.Control 
							type="text" 
							className="login"
							value={mobileNo}
							onChange={(e)=> setMobileNo(e.target.value)}
						/>
					</Form.Group>

				  	<center><Button variant="outline-light" type="submit" className="mainButton">
				    Update Profile
				  	</Button></center>
				</Form>

				{
					pwVisibility === false ? <Fragment></Fragment> :
					<Form onSubmit={(e) => changePassword(e)} className="mt-3 fadeInElements">
						<Form.Group className="mb-3">
							<Form.Control 
								type="password" 
								className="login" 
								value={newPassword}
								placeholder="Enter new password"
								onChange={(e)=> setNewPassword(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group className="mb-3">
								<Form.Control 
									type="password" 
									className="login" 
									value={verifyNewPassword}
									placeholder="Verify new password"
									onChange={(e)=> setVerifyNewPassword(e.target.value)}
									onBlur={(e)=> passwordMatch()}
									required
								/>
						</Form.Group>

						<center><Button 
							variant="outline-light" 
							type="submit" 
							className="mainButton"
							disabled={proceedDisable}
						>Proceed</Button></center>
					</Form>
				}

				<center><Button 
					variant="outline-light" 
					className="mainButton mt-3"
					onClick={()=>changeVisibility()}
					hidden={pwVisibilityBtn}
				>Change Password</Button></center>
			</Fragment>
			)
		}
	}

	return(
		<Container fluid className={getBackground()} style={{overflowY: 'auto'}}>
			<div style={{height:80}}/>
			<Row className="my-auto">
				<Col className={getMargin()} md={getColumn()}>
					<center><h4 className="loginLabel tableBackground my-2 fadeInElements">USER PROFILE</h4></center>
					{
						isProcessing === false ?
						getDisplay()
						:
						<div className="mx-auto my-5">
							<Spinner
						    	as="span"
						    	animation="border"
						    	size="sm"
						    	role="status"
						    	aria-hidden="true"
						   		variant="light"
							/><span className="loading"> Loading details</span></div>
					}
				</Col>
			</Row>
		</Container>
	)
}