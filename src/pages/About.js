import { Container, Row, Col } from 'react-bootstrap';

import picture from '../images/aboutlogo.jpg';
import fb from '../images/facebook.png';
import ig from '../images/instagram.png';
import tw from '../images/twitter.png';
import wa from '../images/whatsapp.png';

export default function About(){
	return(
		<Container fluid className="otherPages pl-0">
			<Row>
				<Col md={4}>
					<img src={picture} alt="aboutlogo" className="img-fluid"/>
				</Col>
				<Col md={8} className="my-auto p-5">
					<h3 className="text-center mb-5"><span className="aboutUs">ABOUT US</span></h3>

					<p>ROLLERS - "A store of I.T. needs, Electronics Appliances, Gadgets, Gizmos, Security Devices and more." </p>

					<p>In 2022 Rollers was born founded by the owner itself Mr. Roldan Ronquillo - Fullstack Javascript Developer/IT</p>

					<h5 className="my-5 text-center">Follow us on social media!</h5>

					<div className="text-center">
						<a href="https://www.facebook.com" target="_blank">
							<img src={fb} alt="facebook" className="socMedLogo mx-3"/>
						</a>
						<a href="https://www.instagram.com" target="_blank">
							<img src={ig} alt="instagram" className="socMedLogo mx-3"/>
						</a>
						<a href="https://www.twitter.com" target="_blank">
							<img src={tw} alt="twitter" className="socMedLogo mx-3"/>
						</a>
						<a href="https://www.whatsapp.com" target="_blank">
							<img src={wa} alt="whatsapp" className="socMedLogo mx-3"/>
						</a>

						<p className="mt-5">Rollers &#169; All rights reserved.</p>
					</div>
				</Col>
			</Row>
		</Container>
	)
}
