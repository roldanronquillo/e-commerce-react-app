import { useParams, useNavigate, Link } from 'react-router-dom';
import { Container, Row, Col, Button, Table, Form, Dropdown, DropdownButton, InputGroup, Spinner } from 'react-bootstrap';
import { useEffect, useState, Fragment } from 'react';

import moment from 'moment';
import toastr from 'toastr';

const admin = localStorage.getItem('admin')

export default function Order(){

	const {orderId} = useParams();

	const [orderedBy, setOrderedBy] = useState("");
	const [totalAmount, setTotalAmount] = useState(0);
	const [orderItems, setOrderItems] = useState([]);
	const [orderStatus, setOrderStatus] = useState("");
	const [paymentStatus, setPaymentStatus] = useState("");
	const [orderFulfilled, setOrderFulfilled] = useState("");
	const [orderedById, setOrderedById] = useState("");
	const [orderPlaced, setOrderPlaced] = useState("");
	const [paymentDate, setPaymentDate] = useState("");
	const [payDisabled, setPayDisabled] = useState(false);
	const [payVisibility, setPayVisibility] = useState(false);
	const [paymentType, setPaymentType] = useState("Select type");
	const [paymentAmount, setPaymentAmount] = useState(0);
	const [refNo, setRefNo] = useState(""); //for payment form
	const [paymentRefNo, setPaymentRefNo] = useState(""); //for the display in order form
	const [paymentId, setPaymentId] = useState("");
	
	const [orderStatusDisabled, setOrderStatusDisabled] = useState(false);
	const [isLoading, setIsLoading] = useState(false);
	const [isProcessing, setIsProcessing] = useState(false);

	const navigate = useNavigate();

	useEffect(() => {
		console.log(admin);
		fetchData();
	},[])

	const fetchData = () => {

		setIsLoading(true);

		fetch(`https://capstone2ecommerceapi.herokuapp.com/api/orders/order/${orderId}`, {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(result => {
			if(result){
				setOrderedBy(result.orderedBy);
				setOrderedById(result.OrderedById);
				setTotalAmount(result.totalAmount);
				setOrderItems(result.orderItems);
				setOrderStatus(result.orderStatus);
				setPaymentStatus(result.paymentStatus);
				setOrderFulfilled(result.orderFulfilled);
				setOrderPlaced(result.purchasedOn);
				setPaymentDate(result.paymentDate);
				setPaymentId(result.paymentId);

				//adjust visibility/disabled fields depending on retrieved values
				if (result.paymentRefNo === undefined){
					setPaymentRefNo(`----------`);
				} else {
					setPaymentRefNo(result.paymentRefNo);
				}				

				if (result.paymentStatus.toLowerCase() === `paid` || result.paymentStatus.toLowerCase() === `cancelled` ){
					setPayDisabled(true);
				}

				if (result.orderStatus.toLowerCase() !== `on-going`){
					setOrderStatusDisabled(true)
				}



			} else {
				toastr.error(result.message);
				navigate('./error');
			}

			setIsLoading(false);
		})
	}

	const getOrderDetails = () => {
		let formattedArr = orderItems.map(product=> {
			return (
			<tr key={product.productId}>
				<td><Link to={`../product/${product.productId}`}>{product.productName}</Link></td>
				<td>&#8369;{product.productPrice.toFixed(2)}</td>
				<td>{product.productQty}</td>
				<td>&#8369;{product.subTotal.toFixed(2)}</td>
			</tr>	

			)
		})

		return formattedArr
	}

	const orderBackground = () =>{
		if (admin === "true"){
			return "adminPage"
		} else {
			return "welcome"
		}
	}

	const addPayment = (e) =>{
		e.preventDefault();
		
		//query
		if (paymentAmount < totalAmount){
			toastr.error(`Cannot pay lower than order amount!`)
		} else {

			setIsProcessing(true);

			fetch(`https://capstone2ecommerceapi.herokuapp.com/api/payment/pay/${orderId}`, {
				method: "POST",
				headers: {
					"Authorization": `Bearer ${localStorage.getItem('token')}`,
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					paymentType: paymentType,
					paymentAmount: paymentAmount,
					paymentRefNo: refNo,
					customerName: `${localStorage.getItem('firstName')} ${localStorage.getItem('lastName')}`
				})
			})
			.then(result => result.json())
			.then(result => {
				
				setIsProcessing(false);

				if (result === true){
					toastr.success(`Successful payment!`);
					fetchData();
				} else {
					toastr.error(result.message);
					setPayVisibility(false);
				}
			})
		}
	}

	const cancelOrder = (e) =>{
		e.preventDefault();

		setIsProcessing(true);
		
		//query
		fetch(`https://capstone2ecommerceapi.herokuapp.com/api/orders/payment-status/${orderId}`, {
			method: "PATCH",
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`,
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				paymentStatus: "Cancelled"
			})
		})
		.then(result => result.json())
		.then(result => {
			
			setIsProcessing(false);

			if (result === true){
				toastr.success(`Order cancelled.`)
				setPayVisibility(false);
				fetchData();
			} else {
				toastr.error(result);
			}
		})
	}

	const showForm = (e) => {
		e.preventDefault(e);
		setPayVisibility(true); 
		setPayDisabled(true)
	}

	const handleDDown = (e) =>{
		setPaymentType(e);
	}

	const handleDDownOrder = (e) => {
		setOrderStatus(e);
	}

	const updateOrderStatus = (e) =>{
		e.preventDefault();

		setIsLoading(true);

		fetch(`https://capstone2ecommerceapi.herokuapp.com/api/orders/order-status/${orderId}`, {
			method: "PATCH",
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`,
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				orderStatus: orderStatus
			})
		})
		.then(result => result.json())
		.then(result => {

			setIsLoading(false);

			if (result === true){
				toastr.success(`Order status updated.`);
				fetchData();
			} else {
				toastr.error(result.message);
			}
		})
	}

	return(
		<Container fluid className={orderBackground()} style={{overflowY:'scroll'}}>
			<div style={{height:60}}/>
			<Row>
				<Col className="my-auto mb-4 mx-auto" md={6}>
					<h4><center><span className="loginLabel blackOpaque fadeInElements">ORDER: {orderId}</span></center></h4>
					{
						isLoading === true? 

						<div className="mx-auto mt-5">
							<Spinner
						    	as="span"
						    	animation="border"
						    	size="sm"
						    	role="status"
						    	aria-hidden="true"
						   		variant="light"
							/><span className="loading"> Retrieving details</span></div>

						:<Table className="mt-3 tableBackground fadeInElements">
						<thead>
						</thead>
						<tbody>
							<tr className="whiteOpaque">
								<td><strong>Customer Name:</strong></td>
								<td><Link to={`../profile/${orderedById}`}>{orderedBy}</Link></td>
								<td><strong>Order Placed:</strong></td>
								<td>{moment(orderPlaced).format('MM/DD/YYYY HH:mm')}</td>
							</tr>
							<tr className="whiteOpaque">
								<td><strong>Order Status:</strong></td>
								<td>{
									admin === "false"? orderStatus : 
									<DropdownButton 
										id="prodDropdown"
										title={orderStatus} 
										disabled={orderStatusDisabled}
										className="btn-block mainButton2"
										onSelect={handleDDownOrder} >
									    <Dropdown.Item eventKey="Cancelled" className="dropdownBg">Cancelled</Dropdown.Item>
									    <Dropdown.Item eventKey="Complete" className="dropdownBg">Complete</Dropdown.Item>
									</DropdownButton>
								}</td>
								<td><strong>Payment Date:</strong></td>
								<td>
									{
										paymentDate === undefined ?
										`--/--/---- --:--`
										: moment(paymentDate).format('MM/DD/YYYY HH:mm')
									}
								</td>
							</tr>
							<tr className="whiteOpaque">
								<td><strong>Payment Status:</strong></td>
								<td>{paymentStatus}</td>
								<td><strong>Order Completed Date:</strong></td>
								<td>
									{
										orderFulfilled === undefined ?
										`--/--/---- --:--`
										: moment(orderFulfilled).format('MM/DD/YYYY HH:mm')
									}
								</td>
							</tr>
							<tr className="whiteOpaque">
								<td><strong>Reference No:</strong></td>
								<td>
									{
										paymentRefNo === `----------`? `----------`
										: <Link to={`../payment/${paymentId}`}>{paymentRefNo}</Link>
									}
								</td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td colSpan="4" className="tableHeader">
									<center><h5>Items:</h5></center>
								</td>
							</tr>
							<tr>
								<td><strong>Product</strong></td>
								<td><strong>Price</strong></td>
								<td><strong>Quantity</strong></td>
								<td><strong>Subtotal</strong></td>
							</tr>
							
							{getOrderDetails()}

							<tr>
								<td colSpan="4"><center><h5></h5></center></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><strong>Total Amount</strong></td>
								<td>&#8369;{totalAmount.toFixed(2)}</td>
							</tr>

						</tbody>
					</Table>
				}
				</Col>
			</Row>
			<Row className="mx-auto">
				<Col className="mx-auto">
					<Row className="mx-auto">
						{
							payVisibility === false? <Fragment></Fragment>
							: <Col md={6} className="tableBackground p-3 my-auto fadeInElements mx-auto">
							<Form onSubmit={(e) => addPayment(e)} className="fadeInElements">
								<Row className="mx-auto">
									<Col md={7}>
										<DropdownButton 
											id="prodDropdown"
											title={paymentType} 
											className="btn-block mainButton2"
											onSelect={handleDDown}
										>
										    <Dropdown.Item eventKey="Cash" className="dropdownBg">Cash</Dropdown.Item>
										    <Dropdown.Item eventKey="GCash" className="dropdownBg">GCash</Dropdown.Item>
										    <Dropdown.Item eventKey="Paymaya" className="dropdownBg">Paymaya</Dropdown.Item>
										    <Dropdown.Item eventKey="Credit Card" className="dropdownBg">Credit Card</Dropdown.Item>
										    <Dropdown.Item eventKey="Bank Transfer - BPI" className="dropdownBg">Bank Transfer - BPI</Dropdown.Item>
										    <Dropdown.Item eventKey="Bank Transfer BDO" className="dropdownBg">Bank Transfer - BDO</Dropdown.Item>
										</DropdownButton>
									</Col>
									<Col md={5}>
										<Form.Group className="mb-3">
											<InputGroup>
												<InputGroup.Text className="igText">&#8369;</InputGroup.Text>
										    		<Form.Control 
										    			type="number" 
										    			placeholder="Amount" 
										    			className="login2"
										    			value={paymentAmount}
										    			onChange={(e)=>setPaymentAmount(e.target.value)}
										    		/>	
											</InputGroup>
										</Form.Group>
									</Col>
								</Row>

							  <Form.Group className="mb-3">
							    	<Form.Control 
							    		type="text"
							    		value={refNo} 
							    		placeholder="Reference Number" 
							    		className="login2"
							    		onChange={(e)=>setRefNo(e.target.value)}
							    	/>
							  </Form.Group>
							  <Button  type="submit" variant="danger">
							    {
							    	isProcessing === false? `Pay` : 
							    		<div><Spinner
							    	    	as="span"
							    	    	animation="border"
							    	    	size="sm"
							    	    	role="status"
							    	    	aria-hidden="true"
							    	   		variant="light"
							    		/> Processing</div>
							    }
							  </Button>
							  <Button className="tableBtn mx-2" onClick={(e)=>cancelOrder(e)}>
							    Cancel Order
							  </Button>
							</Form>
						</Col>		
						}
					</Row>
					<Row className="my-1 mx-auto">
						<Col xs={12}>
							{ admin === "true"? 
							
								<Button
									className="m-2 tableBtn fadeInElements "  
									onClick={(e)=>updateOrderStatus(e)}
									disabled={orderStatusDisabled}
								> Update order status
								</Button>

							: <Fragment>
									<Button 
										className="m-2 fadeInElements" 
										variant="danger" 
										onClick={(e)=>showForm(e)}
										disabled={payDisabled}
									>Proceed to pay</Button>

									<Button 
										className="m-2 btn tableBtn fadeInElements" 
										onClick={()=>navigate('../products/featured')}
									>Shop more</Button>
								</Fragment>}
								<Button 
									className="m-2 btn tableBtn fadeInElements" 
									onClick={()=>navigate('/order-history')}
								>Back to list</Button>
						</Col>
					</Row>
				</Col>
			</Row>
		</Container>
	)
}