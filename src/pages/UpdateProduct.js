import {useState, useEffect, Fragment } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { Container, Row, Form, Col, Button, DropdownButton, Dropdown, InputGroup, Spinner } from 'react-bootstrap';

import ImageCarousel from '../components/ImageCarousel';
import toastr from 'toastr';
import $ from 'jquery';

import bread from '../images/default.png';

export default function UpdateProduct(){
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [category, setCategory] = useState("");
	const [stocks, setStocks] = useState("");
	const [images, setImages] = useState([bread]);

	const [isLoading, setIsLoading] = useState(false);
	const [isUploading, setIsUploading] = useState(false);
	const [isRetrieving, setIsRetrieving] = useState(false);

	const {productId} = useParams();

	const navigate = useNavigate();

	let firstVisit = 0;

	//retrieve product details upon page loading
	useEffect(()=>{

	if (firstVisit === 0){
		firstVisit++;
		if(localStorage.getItem('admin') === "false"){
			navigate('../products');
		} else {
			fetchData();
		}
	}

	},[])

	const fetchData = () => {

		setIsRetrieving(true)

		fetch(`https://capstone2ecommerceapi.herokuapp.com/api/products/product/${productId}`)
		.then(result => result.json())
		.then(result => {

			if (result){
				setName(result.name);
				setDescription(result.description);
				setPrice(result.price);
				setCategory(result.category);
				setStocks(result.stockCount);
				if (result.image.length > 0){
					setImages(result.image);
				}
			}

			setIsRetrieving(false);

		})
	}

	const updateProduct = (e) =>{
		e.preventDefault();

		setIsLoading(true);

		fetch(`https://capstone2ecommerceapi.herokuapp.com/api/products/update/${productId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				category: category,
				stockCount: stocks,
				image: images 
			})
		})
		.then(result => result.json())
		.then(result => {
			// console.log(result)
			if (result){
				toastr.success(`Product updated.`);
				navigate(`../product/${productId}`);
			} else {
				toastr.error(result.message);
			}

			setIsLoading(false);
		})
	}

	const handleDDown = (e) => {
		setCategory(e);
	}

	const uploadPicture = (e) => {
		e.preventDefault();
		
		setIsUploading(true);

		//convert image to reader format for upload to database
		let fReader = new FileReader();
		let tempImgArray = [];

		fReader.readAsDataURL(e.target.files[0]);
		
		fReader.onload = () => {
			tempImgArray.push(fReader.result);
			setImages(tempImgArray);
			setIsUploading(false);
		}

		
	}

	return (
		<Container fluid className="adminPage min-vh-100 mx-auto">
			<Row className="fluid">
				<Col className="mx-auto my-auto">
					<Form onSubmit={(e) => updateProduct(e)} className="fadeInElements">
					<Row className="fluid mx-auto my-auto">
						<Col md={5} className="my-auto ml-auto tableBackground" style={{height:'433px'}}>
						{/*	<ImageCarousel className="fadeInElements"/>*/
							isUploading === false ?
							<Fragment>
								<a href="#">
									<img 
										src={images[0]} 
										alt="product image" 
										className="img-fluid uploadImg my-3"
										onClick={()=>$("#uploadImage").click()}
									/>
								</a> 
								<input 
									type="file" 
									accept="image/*" 
									className="login2" 
									onChange={uploadPicture} 
									id="uploadImage" 
									hidden
								/>
							</Fragment>
							:
							<div>
								<Spinner
								    as="span"
								    animation="border"
								    size="sm"
								    role="status"
								    aria-hidden="true"
								   	variant="light"
								/>
								<h3 className="mt-5">Uploading picture</h3>
							</div>
						}
						</Col>
						<Col className="my-auto tableBackground p-3 mr-auto fadeInElements" md={3} style={{height:'433px'}}>
							<h3 className="loginLabel2 fadeInElements">UPDATE PRODUCT</h3>

							{
								isRetrieving === true? 

								<div className="mx-auto my-5">
									<Spinner
								    	as="span"
								    	animation="border"
								    	size="sm"
								    	role="status"
								    	aria-hidden="true"
								   		variant="light"
									/><span className="loading"> Retrieving details</span></div>

								:
								<div>
									  <Form.Group className="mb-3">
									  	<Form.Control 
									  		type="text" 
									  		className="login2" 
									  		value={name} 
									  		onChange={(e)=> setName(e.target.value)}
									  	/>
									  </Form.Group>

									  <Form.Group className="mb-3">
									  <InputGroup>
									  	<InputGroup.Text className="igText">&#8369;</InputGroup.Text>
									    <Form.Control 
									    	type="number" 
									    	className="login2" 
									    	value={price}
									    	onChange={(e)=> setPrice(e.target.value)}
									    />
									    </InputGroup>
									    </Form.Group>

									  <Form.Group className="mb-3">
									    <Form.Control 
									    	as="textarea" 
									    	className="login2" 
									    	value={description}
									    	onChange={(e)=> setDescription(e.target.value)}
									    	row="3"
									    />
									   
									  </Form.Group>

									  <Form.Group className="mb-3">
											<DropdownButton 
												id="prodDropdown"
												title={category} 
												className="btn-block mainButton2"
												onSelect={handleDDown} >
											    <Dropdown.Item eventKey="electronic_appliances" className="dropdownBg">Electronic_Appliances</Dropdown.Item>
											    <Dropdown.Item eventKey="gadget" className="dropdownBg">Gadget</Dropdown.Item>
											    <Dropdown.Item eventKey="mobile_devices" className="dropdownBg">Mobile_Devices</Dropdown.Item>
											    <Dropdown.Item eventKey="gizmo" className="dropdownBg">Gizmo</Dropdown.Item>
											    <Dropdown.Item eventKey="security_devices" className="dropdownBg">Quick Bites</Dropdown.Item>
											</DropdownButton>
									  </Form.Group>

									  <Form.Group className="mb-3">
									  	<InputGroup>
									  		<InputGroup.Text className="igText">Stocks</InputGroup.Text>
									    <Form.Control 
									    	type="number" 
									    	className="login2" 
									    	value={stocks}
									    	onChange={(e)=> setStocks(e.target.value)}
									    />
									    </InputGroup>
									  </Form.Group>

									  <center>
									  	<Button variant="outline-light" type="submit" className="mainButton2">
									    {
									    	isLoading === false? `Update` : 
									    	<Fragment>
									    	<Spinner
									    	    as="span"
									    	    animation="border"
									    	    size="sm"
									    	    role="status"
									    	    aria-hidden="true"
									    	   	variant="light"
									    	/> Saving</Fragment>
									    }
									  	</Button>
									  	<Button 
									  		variant="outline-light" 
									  		type="submit" 
									  		className="mainButton2 m-2" 
									  		onClick={()=>navigate(`../all-products/cakes`)}>
									  	  Cancel
									  	</Button>
									  </center>
									
								</div>
							}
						</Col>
					</Row>
					</Form>
				</Col>
			</Row>
		</Container>
	)
}