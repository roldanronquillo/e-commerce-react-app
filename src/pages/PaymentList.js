import { useEffect, useState, useContext, Fragment } from 'react';
import { Container, Row, Col, Table, Button, Spinner, Dropdown, DropdownButton } from 'react-bootstrap';
import { useNavigate, Link } from 'react-router-dom';

import Sidebar from '../components/Sidebar';
import UserContext from '../UserContext';
// import ReactPaginate from 'react-paginate';
import toastr from 'toastr';
import BootstrapSwitchButton from 'bootstrap-switch-button-react';
import moment from 'moment';

export default function PaymentList(){

	const [payments, setPayments] = useState([]);

	const [isLoading, setIsLoading] = useState(false);

	const [keyword, setKeyword] = useState("");
	const [category, setCategory] = useState("Search By");

	const navigate = useNavigate();

	//retrieve local storage variables
	const token = localStorage.getItem('token');
	const admin = localStorage.getItem('admin');

	useEffect(()=>{

		if ( token === null ){
			navigate(`/`);
		} else {
			fetchData();
		}
	},[])

	//--------------------------------------------------------------------------------------
	const fetchData = () => {

		let fetchAPI = "";

		if (admin === "true"){
			fetchAPI = `https://capstone2ecommerceapi.herokuapp.com/api/payment/all-payments`;
		} else {
			fetchAPI = `https://capstone2ecommerceapi.herokuapp.com/api/payment/my-payments`
		}

		setIsLoading(true);
		setKeyword("");

		fetch(fetchAPI, {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {

			setIsLoading(false);
				
			if (result.length !== 0) {
				setPayments(result.map(payment => {
					// let name = getName(payment.customerId);
					return(formatData(payment));
				}));
			} else {
				setPayments([<tr><td ColSpan="5">No payments made</td></tr>]);
			}

		})
	}

	//--------------------------------------------------------------------------------------
	const getBackground = () => {
		if(admin === "true"){
			return `adminPage`
		} else {
			return `welcome`
		}
	}

	//--------------------------------------------------------------------------------------
	const formatData = (payment,name) => {

		return(
			<tr key={payment._id} className="p-1">
				<td className="p-2">{moment(payment.paymentDate).format('MM/DD/YYYY HH:mm')}</td>
				<td className="p-2">{payment.paymentRefNo}</td>
				<td className="p-2"><Link to={`../order-details/${payment.orderId}`}>{payment.orderId}</Link></td>
				<td className="p-2"><Link to={`../profile/${payment.customerId}`}>{payment.customerName}</Link></td>
				<td>
					<Button 
						className="tableBtn"
						onClick={()=>navigate(`../payment-details/${payment._id}`)}
					>View</Button>
				</td>
			</tr>
		)
	}

	//--------------------------------------------------------------------------------------
	const handleDDown = (e) => {
		setCategory(e);
	}

	//---------------------------------------------------------------------------------------
	const search = () => {

		setIsLoading(true);
		
		let fetchAPI = "";

		if ( admin === `true` && (category === `By Order Id` || category === `Search By`)){
			fetchAPI = `https://capstone2ecommerceapi.herokuapp.com/api/payment/search-payments/${keyword}`;
		} else if ( admin === `true` && category === `By Reference Number`){
			fetchAPI = `https://capstone2ecommerceapi.herokuapp.com/api/payment/search-ref/${keyword}`;
		} else if ( admin === `true` && category === `By Customer Name`){
			fetchAPI = `https://capstone2ecommerceapi.herokuapp.com/api/payment/search-customer/${keyword}`;
		} else if ( admin === `false` && category === `By Reference Number` ){
			fetchAPI = `https://capstone2ecommerceapi.herokuapp.com/api/payment/search-my-ref/${keyword}`;
		} else if ( admin === `false` && (category === `By Order Id` || category === `Search By`)){
			fetchAPI = `https://capstone2ecommerceapi.herokuapp.com/api/payment/search-my-order/${keyword}`;
		}

		fetch(fetchAPI, {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(result => {

			if (result.length > 0){
				setPayments(result.map(payment => {return(formatData(payment))}));
			} else {
				setPayments([<tr><td colSpan="5">No results found</td></tr>])
			}

			setIsLoading(false);

		})

	}

	//=========================================================================================
	return (
		<Container fluid className={getBackground()} style={{overflowY: 'auto'}}>
			<Row>
				{
					admin === "true" ?
					<Col md={2} className="sideColumn ">
						<Sidebar indicator={`admin`}/>
					</Col>
					: <Fragment></Fragment>
				}
				<Col md={10} className="mx-auto">
					<div style={{height:60}}/>
					<h3><center><span className="loginLabel tableHeader fadeInElements">PAYMENTS LIST</span></center></h3>
					
					<div className="text-right my-4 d-flex justify-content-end">
						<input 
							type="text"
							className="login fadeInElements d-inline form-control"
							style={{width:'300px'}}
							placeholder="Enter keyword"
							onChange={(e)=>setKeyword(e.target.value)}
							value={keyword}
						/>

						<DropdownButton 
							id="prodDropdown"
							title={category} 
							className="mainButton2 ml-2"
							onSelect={handleDDown}
							style={{width:'210px'}} 
						>
						    <Dropdown.Item eventKey="By Order Id" className="dropdownBg">By Order Id</Dropdown.Item>
						    <Dropdown.Item eventKey="By Reference Number" className="dropdownBg">By Reference Number</Dropdown.Item>
						    {
						    	admin === `true` ?
						    	<Dropdown.Item eventKey="By Customer Name" className="dropdownBg">By Customer Name</Dropdown.Item>
						    	: <Fragment></Fragment>
						    }
						</DropdownButton>

						<Button 
							className="ml-2 fadeInElements"
							onClick={()=>search()}
						>Search</Button>
						
						<Button 
							className="ml-2 fadeInElements"
							onClick={()=>fetchData()}
						>Display All</Button>
					</div>

					{
						isLoading === true ? 
						<div className="mx-auto my-5">
							<Spinner
						    	as="span"
						    	animation="border"
						    	size="sm"
						    	role="status"
						    	aria-hidden="true"
						   		variant="light"
							/><span className="loading"> Retrieving list</span></div>
						:
						<Table variant="striped" className="tableBackground mt-4 fadeInElements">
							<thead>
								<tr className="p-2 tableHeader">
									<th className="p-2">Payment Date</th>
									<th className="p-2">Reference Number</th>
									<th className="p-2">Order Number</th>
									<th className="p-2">Customer Name</th>
									<th className="p-2">Details</th>
								</tr>
							</thead>
							<tbody>
								{
									payments
								}
							</tbody>
						</Table>
					}
				</Col>
			</Row>
		</Container>
	)
}