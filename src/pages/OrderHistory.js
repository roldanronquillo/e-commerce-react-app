import { useEffect, useState, Fragment } from 'react';
import { Container, Row, Col, Table, Button, Spinner, Dropdown, DropdownButton } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';

import moment from 'moment';
import Sidebar from '../components/Sidebar';
import UserContext from '../UserContext';
// import ReactPaginate from 'react-paginate';


export default function OrderHistory(){

	const [orders, setOrders] = useState("");

	const [isLoading, setIsLoading] = useState(false);

	const [keyword, setKeyword] = useState("");
	const [category, setCategory] = useState("Search By")

	const navigate = useNavigate();

	//retrieve local storage variables
	const token = localStorage.getItem('token');
	const admin = localStorage.getItem('admin');

	useEffect(()=>{

		if ( token === null ){
			navigate(`/`);
		} else {
			fetchData();
		}

	},[])

	const fetchData = () => {

		let fetchAPI = "";

		if ( admin === "true" ){
			fetchAPI = `https://capstone2ecommerceapi.herokuapp.com/api/orders/all-orders`;
		} else {
			fetchAPI = `https://capstone2ecommerceapi.herokuapp.com/api/orders/my-orders`;
		}

		setIsLoading(true);

		fetch(fetchAPI, {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
				
			if (result) {
				setOrders(result.map(order => {
					return(formatData(order))
				}));
			} else {
				setOrders([<tr><td ColSpan="8">No orders</td></tr>]);
			}

			setIsLoading(false);

		})
	}

	const formatData = (order) => {
		return(
			<tr key={order._id} className="p-1">
				<td className="p-2">{order._id}</td>
				<td className="p-2"><Link to={`../profile/${order.orderedById}`}>{order.orderedBy}</Link></td>
				<td className="p-2">&#8369;{order.totalAmount.toFixed(2)}</td>
				<td className="p-2">{order.paymentStatus}</td>
				<td className="p-2">{order.orderStatus}</td>
				<td className="p-2">{
							order.purchasedOn === undefined ?
							`--/--/---- --:--`
							: moment(order.purchasedOn).format('MM/DD/YYYY HH:mm')
				}</td>
				<td className="p-1">{	
						order.orderFulfilled === undefined ?
						`--/--/---- --:--`
						: moment(order.orderFulfilled).format('MM/DD/YYYY HH:mm')
				}</td>
				<td className="p-2">
					<Button className="tableBtn"
						onClick={()=>navigate(`/order-details/${order._id}`)}
					>View</Button>
				</td>
			</tr>
		)
	}

	const orderBackground = () =>{
		if (admin === "true"){
			return "adminPage"
		} else {
			return "welcome"
		}
	}

	const search = () => {

		let fetchAPI = "";

		//default category to name if none selected
		if ( category === `Search By` ){
			setCategory(`By Product/Customer Name`);
		}

		//admin search by customer name or product
		if (admin === `true` && category === "By Product/Customer Name" ){
			fetchAPI = `https://capstone2ecommerceapi.herokuapp.com/api/orders/search-order/${keyword}`;
			
		//admin search by Id
		} else if (admin === `true` && category === `By Id`){
			fetchAPI = `https://capstone2ecommerceapi.herokuapp.com/api/orders/order/${keyword}`;

		//user search by Id
		} else if ( admin === `false` && category === `By Id`) {
			fetchAPI = `https://capstone2ecommerceapi.herokuapp.com/api/orders/search-my-order-id/${keyword}`;

		//user search by customer name or product
		} else if ( admin === `false` && category === `By Product/Customer Name` ) {
			fetchAPI = `https://capstone2ecommerceapi.herokuapp.com/api/orders/search-my-order/${keyword}`;
		}

		setIsLoading(true);

		fetch(fetchAPI, {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(result => {

			if (result.length > 0){
				setOrders(result.map(order=>formatData(order)))
			} else {
				//single object result
				if (result.name === undefined){

					if(result._id !== undefined){
						setOrders([formatData(result)])
					//empty array
					} else {
						setOrders([<tr><td colSpan="8">No results found</td></tr>]);
					}

				} else {	
					setOrders([<tr><td colSpan="8">No results found</td></tr>]);
				}
			}

			setIsLoading(false);

		})
	}

	const handleDDown = (e) => {
		setCategory(e);
	}

	return(
		<Container fluid className={orderBackground()} style={{overflowY:'scroll'}}>
			<Row>
				{
					admin === "true" ?
					<Col md={2} className="sideColumn ">
						<Sidebar indicator={`admin`}/>
					</Col>
					: <Fragment></Fragment>
				}
				<Col md={10} className="mx-auto">
					<div style={{height:60}}/>
					<h3><center><span className="loginLabel tableHeader fadeInElements">ORDER HISTORY</span></center></h3>
					
					<div className="text-right my-4 d-flex justify-content-end">
						<input 
							type="text"
							className="login fadeInElements d-inline form-control"
							style={{width:'300px'}}
							placeholder="Enter keyword"
							onChange={(e)=>setKeyword(e.target.value)}
						/>

						<DropdownButton 
							id="prodDropdown"
							title={category} 
							className="mainButton2 ml-2"
							onSelect={handleDDown}
							style={{width:'300px'}} 
						>
						    <Dropdown.Item eventKey="By Product/Customer Name" className="dropdownBg">By Product/Customer Name</Dropdown.Item>
						    <Dropdown.Item eventKey="By Id" className="dropdownBg">By Id</Dropdown.Item>
						</DropdownButton>

						<Button 
							className="ml-2 fadeInElements"
							onClick={()=>search()}
						>Search</Button>
						
						<Button 
							className="ml-2 fadeInElements"
							onClick={()=>fetchData()}
						>Display All</Button>
					</div>

					{ 
						isLoading === true?
						<div className="mx-auto my-5">
							<Spinner
						    	as="span"
						    	animation="border"
						    	size="sm"
						    	role="status"
						    	aria-hidden="true"
						   		variant="light"
							/><span className="loading"> Retrieving list</span></div> :

							<Table variant="striped" className="tableBackground mt-4 fadeInElements">
								<thead>
									<tr className="p-2 tableHeader">
										<th className="p-2">Order Id</th>
										<th className="p-2">Customer Name</th>
										<th className="p-2">Order Amount</th>
										<th className="p-2">Payment Status</th>
										<th className="p-2">Order Status</th>
										<th className="p-2">Order Date</th>
										<th className="p-2">Order Completed Date</th>
										<th className="p-2">Details</th>
									</tr>
								</thead>
								<tbody>
										{
											orders
											/*currentPageData*/
										}
								</tbody>
							</Table>
					}
					<center>
					{
						admin === "true" ? <Fragment></Fragment>
						: <Fragment>
							<Button 
								variant="dark"
								className=" btn tableBtn m-2 d-block fadeInElements"
								onClick={()=>navigate('../products')}
							>Back to products</Button>
						</Fragment> 

					}</center>
				</Col>
				<Col xs={12}>
				</Col>
			</Row>
		</Container>
	)
}