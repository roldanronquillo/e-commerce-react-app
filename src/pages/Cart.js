import { useEffect, useState } from 'react';
import {Table, Container, Row, Col, Button, InputGroup, FormControl, Spinner } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';

// import UserContext from '../UserContext';
// import QuantitySelector from '../components/QuantitySelector';
import ReactPaginate from 'react-paginate';
import toastr from 'toastr';

const admin = localStorage.getItem('admin');

export default function Cart(){

	const [total, setTotal] = useState(0);
	const [printDetails, setPrintDetails] = useState([]);
	const [orders, setOrders] = useState([]);
	const [quantity, setQuantity] = useState(0);

	const [isLoading, setIsLoading] = useState(false);

	// const [stockCount, setStockCount] = useState(0);
	// const [products, setProducts] = useState([]);
	let prodList = [];

	const navigate = useNavigate();

	// const {user} = useContext(UserContext);

	//variables for pagination
	const [currentPage, setCurrentPage] = useState(0);
	const [itemsPerPage, setItemsPerPage] = useState(0);

	const screenSize = window.innerWidth;

	//get cart contents from local storage
	useEffect(()=>{

		if(admin === "false"){
			cartDetails();
			fetchProducts();
		} else if(admin === "true") {
			navigate(`../all-products`);
		} else {
			navigate(`../products`);
		}

	// 	//adjust items depending on screenSize
	// 	if (screenSize <= 1440){
	// 		setItemsPerPage(4);
	// 	} else if (screenSize > 1440 && screenSize < 2560 ){
	// 		setItemsPerPage(7);
	// 	} else {
	// 		setItemsPerPage(10);
	// 	}

	},[])

	//retrieve cart details from localStorage
	const cartDetails = () => {
	 	
	 	if (localStorage.getItem('orders')){

	 		let items = JSON.parse(localStorage.getItem('orders'));
	 		let tempTotal = 0;
	 		setOrders(items);	 		

	 		setPrintDetails(
	 			items.map(product=>{
	 				tempTotal += product.subTotal;
	 				setQuantity(product.productQty);

	 				return(
	 					<tr key={product.productId}>
	 							
	 						<td><Link to={`../product/${product.productId}`}>{product.productName}</Link></td>
	 						
	 						<td>&#8369;{product.productPrice.toFixed(2)}</td>
	 						
	 						<td className="p-1 mt-2" style={{width:120}}>{/*{product.productQty}*/}
	 							<InputGroup className="mb-3 cartQty" style={{width:120}}>
	 							  <Button variant="light" onClick={()=>decQty(product.productId)}>-</Button>
	 							  <FormControl
	 							    className="login cartNumberField no-wrap"
	 							    type="number"
	 							    value={product.productQty}
	 							    onChange={(e)=> setQuantity(e.target.value)}
	 							    disabled
	 							  />
	 							  <Button variant="light" onClick={()=>addQty(product.productId)}>+</Button>
	 							</InputGroup>
	 						</td>
	 						
	 						<td>&#8369;{product.subTotal.toFixed(2)}</td>
	 						
	 						<td>
	 							<Button 
	 								variant="danger"
	 								onClick={()=>removeItem(product.productId)}
	 							>X</Button>
	 						</td>
	 					
	 					</tr>
	 				)
	 			})
	 		)
	 		setTotal(tempTotal)
	 	} else {
	 		let temp = [<tr><td colSpan="5"><center>Empty</center></td></tr>]; 
	 		setPrintDetails(temp);
	 	}
	 }


	const fetchProducts = () => {
	 	fetch(`https://capstone2ecommerceapi.herokuapp.com/api/products/`)
	 	.then(result => result.json())
	 	.then(result => {
	 		if (result.message !== null){
	
	 			prodList = result;

	 		} else {
	 			toastr.error(result.message)
	 		}
	 	})
	 }

	const removeItem = (productId) => {

	 	let items =JSON.parse(localStorage.getItem('orders'));
	 	let index = items.findIndex(product => product.productId === productId);

	 	if (index >= 0){
	 		items.splice(index,1);
	 		setOrders(items)
	 		localStorage.setItem('orders',JSON.stringify(items));
	 	}

	 	cartDetails();
	}

	const placeOrder = () => {

	 	if (orders[0] === undefined){
			// alert(`Cannot check-out empty cart!`);
			toastr.warning("Cannot check-out empty cart!");
		} else {

			setIsLoading(true);

			fetch(`https://capstone2ecommerceapi.herokuapp.com/api/orders/create-order`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					totalAmount: total,
					orderItems:orders
				})
			})
			.then(result => result.json())
			.then(result => {

				setIsLoading(false);

				if (result){
					// console.log(result._id);
					emptyCart();
					// alert('Order placed.');
					toastr.success(`Order placed.`);
					navigate(`../order-details/${result._id}`);
				} else {
					alert(result.message);
				}
			})
		}
	 	

	 }

	const emptyCart = () => {
	 	localStorage.removeItem('orders');
	 	setTotal(0);
	 	setOrders([]);
	 	cartDetails();
	 }

	const addQty = (productId) => {

	 	let items =JSON.parse(localStorage.getItem('orders'));

	 	//find index of product from cart array
	 	let index = items.findIndex(product => product.productId === productId);

	 	//index of product from product list for comparison of stocks
	 	let iProduct = prodList.findIndex(product => product._id === productId);

	 	console.log(`prodList`,prodList[iProduct].stockCount)
	 	console.log(`cart`, items[index].productQty)

	 	if (prodList[iProduct].stockCount > items[index].productQty ){
	 		items[index].productQty += 1;
	 		items[index].subTotal = items[index].productQty * items[index].productPrice;

	 		setOrders(items);
	 		localStorage.setItem('orders',JSON.stringify(items));

	 		cartDetails();
	 	} else {
	 		toastr.error(`Cannot add more. This is the last stock for this item.`);
	 	}
	}

	const decQty = (productId) => {

	 	let items =JSON.parse(localStorage.getItem('orders'));
	 	let index = items.findIndex(product => product.productId === productId);

	 	if ( items[index].productQty > 1 ){

	 		items[index].productQty -= 1;
	 		items[index].subTotal = items[index].productQty * items[index].productPrice;
	 		
	 		setOrders(items);
	 		localStorage.setItem('orders',JSON.stringify(items));

	 		cartDetails();
	 	}
	}

	 // const handlePageClick = ({selected: selectedPage}) => {
	 // 	setCurrentPage(selectedPage);
	 // }

	 // const offset = currentPage * itemsPerPage;

	 //total pages
	 // const pageCount = Math.ceil(printDetails.length / itemsPerPage);

	 //slice array to display number of items per page
	 // const currentPageData = printDetails.slice(offset, offset + itemsPerPage);

	return (
		<Container fluid className="welcome" style={{overflowY: 'auto'}}>
			<div style={{height:80}}/>
			<Row className="mx-auto">
				<Col className="my-auto ml-md-5" md={6}>
					<center><h4><span className="loginLabel fadeInElements">MY CART</span></h4></center>
					
					<Table className="tableBackground mt-4 fadeInElements">
						<thead className="tableHeader">
							<tr>
								<th>Product</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Subtotal</th>
								<th>Remove</th>
							</tr>
						</thead>
						<tbody>
							{
								/*currentPageData*/
								printDetails
							}

							<tr className="tableHeader">
								<td colSpan="3" align="right"><strong>Total Amount</strong></td>
								<td>&#8369;{total.toFixed(2)}</td>
								<td></td>
							</tr>
						</tbody>
					</Table>
					{
						/*printDetails.length < 4 ? <></>
						: <ReactPaginate
							previousLabel={"<"}
							nextLabel={">"}
							pageCount={pageCount}
							containerClassName="pagination"
							onPageChange={()=>handlePageClick}
							pageClassName={"ml-1 px-2 blackOpaque"}
							nextClassName={"ml-1 px-2 whiteOpaque"}
							previousClassName={"ml-1 px-2 whiteOpaque"}
							previousLinkClassName={"sideLink"}
							nextLinkClassName={"sideLink"}
							disabledLinkClassName={"ml-1 disabledPageLink"}
							activeClassName={"tableHeader"}
						/> */
					}
					<Button
						onClick={()=> emptyCart()}
						className="tableBtn mx-3 my-2 fadeInElements"
					>Empty cart</Button>
					<Button
						onClick={()=> placeOrder()}
						variant="light"
						className="my-2 fadeInElements"
					>
					{
						isLoading === false? `Checkout`
						:<div><Spinner
						    	as="span"
						    	animation="border"
						    	size="sm"
						    	role="status"
						    	aria-hidden="true"
						   		variant="dark"
							/> Processing</div>
					}
					</Button>
				</Col>
			</Row>
		
		</Container>
	)
}