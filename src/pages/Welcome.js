import {Container, Row, Col, Button} from 'react-bootstrap';
import logo from '../images/logo.png';
import { useEffect } from 'react';
import {useNavigate} from 'react-router-dom';

// import UserContext from '../UserContext';

import About from './About';

export default function Welcome(){

	// const {user} = useContext(UserContext);
	const navigate = useNavigate();

	useEffect(()=>{
		if(localStorage.getItem('admin') === "true" ){
			navigate(`../all-products`);
		} else if(localStorage.getItem('admin') === "false" ){
			navigate(`../products`);
		}
	},[]);

	return (
		<Container fluid>
			<Row className="my-auto welcome">
				<Col className="my-auto ml-5" md={5} xs={12}>
					<h3 className="fadeInElements">"SHOP NOW!!! @ "</h3>
					{/*<h1><span className="companyName">ROLLERS</span></h1>*/}
					<img src={logo} alt="" className="img-fluid mb-4 fadeInElements"/>
					<Button variant="outline-light" href="/login" className="m-2 mt-4 mainButton fadeInElements">Login</Button>
					<Button variant="outline-light" href="/register" className="m-2 mt-4 mainButton fadeInElements">Register</Button>
				</Col>
			</Row>
			<Row>
				<About />
			</Row>
		</Container>
	)
}