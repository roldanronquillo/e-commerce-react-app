import {Container, Row, Col, Button, Form, Spinner} from 'react-bootstrap';
import {useState, useEffect, useContext, Fragment } from 'react';
import {useNavigate} from 'react-router-dom';

import UserContext from '../UserContext';
import toastr from 'toastr';

export default function Login(){

	//React hooks ---------------------------------------------------------
	const [email, setEmail] = useState("");
	const [pw, setPw] = useState("");
	const [isDisabled, setIsDisabled] = useState(true);
	const [isLoading, setIsLoading] = useState(false);

	const navigate = useNavigate();

	const { dispatch } = useContext(UserContext);

	//UseEffect hook ------------------------------------------------------
	useEffect(() => {
		if(email!=="" && pw!==""){
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email,pw])

	//Event listener -------------------------------------------------------
	const authenticate = (e) => {
		e.preventDefault();
		
		setIsLoading(true)

		fetch('https://capstone2ecommerceapi.herokuapp.com/api/users/login', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: pw
			})
		})
		.then(result => result.json())
		.then(result => {
			
			setIsLoading(false);

			//successfully logged in
			if(result.token){

				localStorage.setItem('token', result.token);

				// retrieve user details
				fetch(`https://capstone2ecommerceapi.herokuapp.com/api/users/my-profile`, {
					method: "GET",
					headers: {
						"Authorization": `Bearer ${result.token}`
					}
				})
				.then(result => result.json())
				.then(result => {

					//store user data
					localStorage.setItem('admin', result.isAdmin);
					localStorage.setItem('firstName', result.firstName);
					localStorage.setItem('lastName', result.lastName);

					//restore state to default
					setEmail("");
					setPw("");

					if(result.isAdmin === false){
						dispatch({type:"USER", name:result.firstName, admin: result.isAdmin});
						navigate('/products');
					} else if (result.isAdmin === true){
						dispatch({type:"ADMIN", name:result.firstName, admin: result.isAdmin});
						navigate('../all-products');
					}
					
				})
				
			} else {
				toastr.error(result.message);
				// alert(result.message);
			}

		});

	}

	return(
		<Container fluid className="welcome min-vh-100">
			{/*<h4 className="text-center p-3 mx-auto"><span className="logo">THE BAKERY</span></h4>*/}
			<div style={{height:50}}/>
			<Row className="my-auto loginRow">
				<Col className="my-auto loginMargin" md={3}>
					<h1 className="loginLabel fadeInText">LOGIN</h1>
					<p className="fadeInElements">Not registered? Sign up <a href="/register" id="loginLink" className="fadeInElements">here</a></p>
					<Form onSubmit={(e) => authenticate(e)} className="fadeInElements">
					  <Form.Group className="mb-3">
					  	<Form.Control 
					  		type="email" 
					  		placeholder="Enter email" 
					  		className="login" 
					  		value={email} 
					  		onChange={(e)=> setEmail(e.target.value)}
					  	/>
					  </Form.Group>

					  <Form.Group className="mb-3">
					    <Form.Control 
					    	type="password" 
					    	placeholder="Password" 
					    	className="login" 
					    	id="password"
					    	value={pw}
					    	onChange={(e)=> setPw(e.target.value)}/>
					  </Form.Group>
					  <center><Button variant="outline-light" type="submit" disabled={isDisabled} className="mainButton">
					    {
					    	isLoading === false? <span>Submit</span> : 
					    	<Fragment>
					    	<Spinner
					    	    as="span"
					    	    animation="border"
					    	    size="sm"
					    	    role="status"
					    	    aria-hidden="true"
					    	   	variant="light"
					    	/><span> Loading</span></Fragment>
					    }
					  </Button></center>
					</Form>
				</Col>
			</Row>
		</Container>
	)
}