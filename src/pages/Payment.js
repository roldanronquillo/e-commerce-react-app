import { Container, Row, Col, Button, Table, Spinner } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import { useEffect, useState } from 'react'

import moment from 'moment';
import toastr from 'toastr';

const token = localStorage.getItem('token');
const admin = localStorage.getItem('admin');

export default function Payment(){

	//declare payment variables
	const [paymentDate, setPaymentDate] = useState("");
	const [paymentType, setPaymentType] = useState("");
	const [orderId, setOrderId] = useState("");
	const [customerId, setCustomerId] = useState("");
	const [customerName, setCustomerName] = useState("")
	const [paymentAmount, setPaymentAmount] = useState(0);

	const [isLoading, setIsLoading] = useState(false);

	const { paymentId } = useParams();

	const navigate = useNavigate();

	useEffect(()=> {

		// console.log(token)
		console.log(token === null)
		if ( token === null ){
			navigate(`/`);
		} else {
			fetchData();
		}
	},[])

	const fetchData = () => {

		setIsLoading(true);

		//retrieve payment data
		fetch(`https://capstone2ecommerceapi.herokuapp.com/api/payment/payment-details/${paymentId}`, {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			} 
		})
		.then(result => result.json())
		.then(result => {

			setIsLoading(false);

			if(result.message === undefined){

				//assign results to variables
				setPaymentDate(result.paymentDate);
				setPaymentType(result.paymentType);
				setOrderId(result.orderId);
				setCustomerId(result.customerId);
				setCustomerName(result.customerName)
				setPaymentAmount(result.paymentAmount);

			} else {
				toastr.error(result.message);
			}
		})
	}

	const getBackground = () => {
		if(admin === "true"){
			return `adminPage`
		} else {
			return `welcome`
		}
	}

	return (
		<Container fluid className={getBackground()} style={{overflowY:'scroll'}}>
			<div style={{height:60}}/>
			<Row className="my-auto">
				<Col className="my-auto mb-4 mx-auto" md={6}>
					<h4><center><span className="loginLabel tableHeader fadeInElements">PAYMENT ID: {paymentId}</span></center></h4>

					{
						isLoading === true ? 
						<div className="mx-auto my-5">
							<Spinner
						    	as="span"
						    	animation="border"
						    	size="sm"
						    	role="status"
						    	aria-hidden="true"
						   		variant="light"
							/><span className="loading"> Loading details</span></div>
						: 
						<Table className="mt-3 tableBackground fadeInElements">
							<thead>
							</thead>
							<tbody>
								<tr>
									<td><strong>Payment Type:</strong></td>
									<td>{paymentType}</td>
									<td><strong>Payment Amount:</strong></td>
									<td>&#8369;{paymentAmount.toFixed(2)}</td>
								</tr>
								<tr>
									<td><strong>Order ID:</strong></td>
									<td><Link to={`../order-details/${orderId}`}>{orderId}</Link></td>
									<td><strong>Customer Name:</strong></td>
									<td><Link to={`../profile/${customerId}`}>{customerName}</Link></td>
								</tr>
								<tr>
									<td><strong>Payment Date:</strong></td>
									<td>{moment(paymentDate).format('MM/DD/YYYY HH:mm')}</td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</Table>
					}

					<Button className="tableBtn m-3" onClick={()=>navigate(`../all-payments`)}>Back to payment list</Button>
				</Col>
			</Row>
		</Container>
	)
}