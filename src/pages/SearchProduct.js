import {Container, Row, Col, Form, Button, Spinner } from 'react-bootstrap';
import { useState } from 'react';

import Sidebar from '../components/Sidebar';
import ProductCard from '../components/ProductCard';


export default function SearchProduct(){

	const [products, setProducts] = useState([]);
	const [keyword, setKeyword] = useState("");
	const [isLoading, setIsLoading] = useState(false);

	const search = () => {

		setIsLoading(true);

		fetch(`https://capstone2ecommerceapi.herokuapp.com/api/products/search-products/${keyword}`)
		.then(result=>result.json())
		.then(result => {

			setProducts(
				result.map(product=>{
					return <ProductCard key={product._id} productProp={product}/>
				})
			)

			setIsLoading(false);

		})

	}

	return(
		<Container fluid className="welcome" style={{overflowY:'auto'}}>
			<Row>
				<Col md={2} className="p-0">
					<Sidebar indicator={`products`}/>
				</Col>
				<Col md={10} className="mx-auto">
					<div style={{height:50}}/>
					
					<Row>
						<h1 className="my-4 mx-auto">
							<span className="loginLabel productAnimate">
								SEARCH PRODUCTS
							</span>
						</h1>
					</Row>

					<Row className="my-3">
						<Form className="mx-auto fadeInElements">
							<Row className="mx-auto">
								<Col xs={10}>
								 	<input
								 		type="text"
								 		className="login form-control mt-2"
								 		placeholder="Enter keyword"
								 		onChange={(e)=>setKeyword(e.target.value)}
								 		onBlur={()=>search()}
								 		value={keyword}
								 	/>
								</Col>
								<Col xs={10} md={2}>
									<Button
										className="tableBtn mt-2"
										onClick={()=>search()}
									>Search</Button>
								</Col>
							</Row>
						</Form>
					</Row>

					<Row className="d-flex flex-wrap">
					{
						isLoading === false?
						products
						: 					    
						<div className="mx-auto mt-5">
						    <Spinner
						    	as="span"
						    	animation="border"
						    	size="sm"
						    	role="status"
						    	aria-hidden="true"
						    	variant="light"
						    /><span className="loading"> Retrieving products</span></div>
					}
					</Row>

				</Col>
			</Row>
		</Container>
	)
}