import {Container, Row, Col, Button, Table, Spinner} from 'react-bootstrap';
import {useState, useEffect, useContext } from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom';

import Sidebar from '../components/Sidebar';
import UserContext from '../UserContext';
import toastr from 'toastr';
import BootstrapSwitchButton from 'bootstrap-switch-button-react';

const admin = localStorage.getItem('admin');
const firstName = localStorage.getItem('firstName');

export default function AdminSearch(){

	const [products, setProducts] = useState([]);

	const [isLoading, setIsLoading] = useState(false);

	const [keyword, setKeyword] = useState("");

	const navigate = useNavigate();
	const {category} = useParams();
	const { state, dispatch } = useContext(UserContext);

	//retrieve products upon page load
	useEffect(()=>{

		if (admin === "true"){
			dispatch({type:"ADMIN", name: firstName, admin: true});
		} else if (admin === "false") {
			dispatch({type:"USER", name: firstName, admin: false});
		}

		if (state.admin === false){
			navigate(`../products`);
		}

	},[])

	const generateTableContents = (product) => {
		return(
			<tr key={product._id}>
				<td style={{display:'none'}}>{product._id}</td>
				<td>{product.name}</td>
				<td>{product.category}</td>
				<td>&#8369; {product.price.toFixed(2)}</td>
				<td>{product.stockCount}</td>
				<td>{product.quantitySold}</td>
				<td>
					<BootstrapSwitchButton
						checked={product.isActive}
						onlabel="Yes"
						offlabel="No"
						onChange={() => updateStatus(product._id, product.stockCount)}
					/>
				</td>
				<td>
					<BootstrapSwitchButton
						checked={product.isFeatured}
						onlabel="Yes"
						offlabel="No"
						onChange={() => updateFeatured(product._id)}
					/>
				</td>
				<td>
					<Button 
						className="tableBtn" 
						onClick={()=>{
							navigate(`../update-product/${product._id}`)}}
					>Update</Button>

					<Button 
						className="tableBtn ml-2" 
						onClick={()=>{
							navigate(`../product/${product._id}`)}}
					>View</Button>					
				</td>
			</tr>
		)
	}

	//function to update featured tag
	const updateFeatured = (productId) => {
		
		fetch(`https://capstone2ecommerceapi.herokuapp.com/api/products/update-featured/${productId}`, {
			method: "PATCH",
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(result => {
			if(result){
				toastr.success(`Product is featured.`)
			} else {
				toastr.error(result.message);
			}
		})
	}

	//function to update product status
	const updateStatus = (productId, qty) => {
	
		if (qty <= 0){
			toastr.error(`Cannot activate products that are out of stock!`)
		} else {
			fetch(`https://capstone2ecommerceapi.herokuapp.com/api/products/active-status/${productId}`, {
				method: "PATCH",
				headers: {
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(result => result.json())
			.then(result => {
				if(result){
					toastr.success(`Successfully updated product.`);
				} else {
					toastr.error(result.message);
				}
			})
		}
	}

	const search = () => {

		setIsLoading(true);

		fetch(`https://capstone2ecommerceapi.herokuapp.com/api/products/admin-search-all/${keyword}`, {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(result => {

			if (result.length > 0){
				setProducts(result.map(product=>generateTableContents(product)))
			} else {
				setProducts([
					<tr>
						<td colSpan="9">No results found</td>
					</tr>
				]);
			}

			setIsLoading(false);

		})
	}

		return(
			<Container fluid className="adminPage" style={{overflowY:'auto'}}>
				<Row>
					<Col md={2} className="sideColumn ">
						<Sidebar indicator={`admin`}/>
					</Col>

					<Col md={10}>
						<div style={{height:60}}/>
						<h3><span className="loginLabel tableHeader fadeInElements">SEARCH PRODUCTS</span></h3>
						<div className="my-4">
							<input 
								type="text"
								className="login fadeInElements d-inline form-control"
								style={{width:'300px'}}
								placeholder="Enter keyword"
								onChange={(e)=>setKeyword(e.target.value)}
							/>
							<Button 
								className="ml-2 fadeInElements"
								onClick={()=>search()}
							>Search</Button>
						</div>
						{
							isLoading === true ?

							<div className="mx-auto my-5">
								<Spinner
							    	as="span"
							    	animation="border"
							    	size="sm"
							    	role="status"
							    	aria-hidden="true"
							   		variant="light"
								/><span className="loading"> Retrieving list</span></div>

							: 
							<Table className="tableBackground fadeInElements" responsive >
								<thead>
									<tr className="tableHeader">
										<th style={{display:'none'}}>ID</th>
										<th>Name</th>
										<th>Category</th>
										<th>Price</th>
										<th>Stocks</th>
										<th># Sold</th>
										<th>Display</th>
										<th>Featured</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									{
										products
									}
								</tbody>
							</Table>
						}

					</Col>
				</Row>	
			
			</Container>
		)
}