import { useEffect, useState, useContext, Fragment } from 'react';
import { Container, Row, Col, Table, Button, Spinner } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

import Sidebar from '../components/Sidebar';
import UserContext from '../UserContext';
// import ReactPaginate from 'react-paginate';
import toastr from 'toastr';
import BootstrapSwitchButton from 'bootstrap-switch-button-react';

export default function UserList(){

	const [users, setUsers] = useState("");

	const [isLoading, setIsLoading] = useState(false);

	const [keyword, setKeyword] = useState("");

	const navigate = useNavigate();
	// const { state } = useContext(UserContext);

	//retrieve local storage variables
	const token = localStorage.getItem('token');
	const admin = localStorage.getItem('admin');

	//variables for pagination
	// const [currentPage, setCurrentPage] = useState(0);
	// const [itemsPerPage, setItemsPerPage] = useState(0);

	// const screenSize = window.innerWidth;

	useEffect(()=>{

		if ( admin !== "true" ){
			navigate(`/`);
		} else {
			fetchData();
		}

		// if (screenSize <= 1440){
		// 	setItemsPerPage(7);
		// } else if (screenSize > 1440 && screenSize < 2560 ){
		// 	setItemsPerPage(10);
		// } else {
		// 	setItemsPerPage(16);
		// }

	},[])

	const fetchData = () => {

		setIsLoading(true);
		setKeyword("");

		fetch(`https://capstone2ecommerceapi.herokuapp.com/api/users/all-users`, {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
				
			if (result) {
				setUsers(result.map(user => {
					return(formatData(user))
				}));
			} else {
				setUsers([<tr><td ColSpan="8">User list empty</td></tr>]);
			}

			setIsLoading(false);

		})
	}

	const updateAdminStatus = (userId) => {
		fetch(`https://capstone2ecommerceapi.herokuapp.com/api/users/change-user-status/${userId}`, {
			method: "PATCH",
			headers: { "Authorization": `Bearer ${token}` }
		})
		.then(result => result.json())
		.then(result => {
			if (result.message !== null){
				toastr.success(`Administrator status updated successfully.`);
				// alert(`Administrator status updated successfully.`);
				fetchData();
			} else {
				toastr.error(result.message);
				// alert(result.message);
			}
		})
	}

	const updateActiveStatus = (userId) => {
		fetch(`https://capstone2ecommerceapi.herokuapp.com/api/users/user-access/${userId}`, {
			method: "PATCH",
			headers: { "Authorization": `Bearer ${token}` }
		})
		.then(result => result.json())
		.then(result => {
			if (result.message !== null){
				toastr.success(`Active status updated successfully.`);
				// alert(`Active status updated successfully.`);
				fetchData();
			} else {
				toastr.error(result.message);
				// alert(result.message);
			}
		})
	}

	const formatData = (user) => {
		return(
			<tr key={user._id} className="p-1">
				<td className="p-2">{user.firstName}</td>
				<td className="p-2">{user.lastName}</td>
				<td className="p-2">{user.email}</td>
				<td className="p-2">{user.mobileNo}</td>
				<td className="p-2">
					<BootstrapSwitchButton
						checked={user.isAdmin}
						onlabel="Admin"
						offlabel="User"
						width={100}
						onChange={() => updateAdminStatus(user._id)}
					/>
				</td>
				<td className="p-2">
					<BootstrapSwitchButton
						checked={user.isActive}
						onlabel="Active"
						offlabel="Inactive"
						width={100}
						onChange={() => updateActiveStatus(user._id)}
					/>
				</td>
				<td className="p-2">
					<Button className="tableBtn"
						onClick={()=>navigate(`../profile/${user._id}`)}
					>View</Button>
				</td>
			</tr>
		)
	}

	const search = () => {

		setIsLoading(true);

		fetch(`https://capstone2ecommerceapi.herokuapp.com/api/users/search-user/${keyword}`, {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(result => {

			if (result.length > 0){
				setUsers(result.map(user=>formatData(user)))
			} else {
				setUsers([
					<tr>
						<td colSpan="7">No results found</td>
					</tr>
				]);
			}

			setIsLoading(false);

		})
	}

	// const handlePageClick = ({selected: selectedPage}) => {
	// 	setCurrentPage(selectedPage);
	// }

	// const offset = currentPage * itemsPerPage;

	//total pages
	// const pageCount = Math.ceil(users.length / itemsPerPage);

	//slice array to display number of items per page
	// const currentPageData = users.slice(offset, offset + itemsPerPage)

	return(
		<Container fluid className="adminPage" style={{overflowY: 'auto'}}>
			<Row>
				{
					admin === "true" ?
					<Col md={2} className="sideColumn ">
						<Sidebar indicator={`admin`}/>
					</Col>
					: <Fragment></Fragment>
				}
				<Col md={10} className="mx-auto">
					<div style={{height:60}}/>
					<h3><center><span className="loginLabel tableHeader fadeInElements">USERS LIST</span></center></h3>
					<div className="text-right my-4">
						<input 
							type="text"
							className="login fadeInElements d-inline form-control"
							style={{width:'300px'}}
							placeholder="Search name"
							onChange={(e)=>setKeyword(e.target.value)}
							value={keyword}
						/>
						<Button 
							className="ml-2 fadeInElements"
							onClick={()=>search()}
						>Search</Button>
						<Button 
							className="ml-2 fadeInElements"
							onClick={()=>fetchData()}
						>Display All</Button>
					</div>
					{
						isLoading === true ?
							<div className="mx-auto my-5">
								<Spinner
							    	as="span"
							    	animation="border"
							    	size="sm"
							    	role="status"
							    	aria-hidden="true"
							   		variant="light"
							/><span className="loading"> Retrieving list</span></div>
						:
							<Table variant="striped" className="tableBackground mt-4 fadeInElements">
								<thead>
									<tr className="p-2 tableHeader">
										<th className="p-2">First Name</th>
										<th className="p-2">Last Name</th>
										<th className="p-2">Email Address</th>
										<th className="p-2">Mobile Number</th>
										<th className="p-2">Access Rights</th>
										<th className="p-2">Active</th>
										<th className="p-2">Action</th>
									</tr>
								</thead>
								<tbody>
									{
										users
										/*currentPageData*/
									}
								</tbody>
							</Table>
					}
{/*					<center><ReactPaginate
						previousLabel={"<"}
						nextLabel={">"}
						pageCount={pageCount}
						containerClassName="pagination"
						onPageChange={()=>handlePageClick}
						pageClassName={"ml-1 px-2 blackOpaque"}
						nextClassName={"ml-1 px-2 whiteOpaque"}
						previousClassName={"ml-1 px-2 whiteOpaque"}
						previousLinkClassName={"sideLink"}
						nextLinkClassName={"sideLink"}
						disabledLinkClassName={"ml-1 disabledPageLink"}
						activeClassName={"tableHeader"}
					/></center>*/}
				</Col>
			</Row>
		</Container>
	)

}