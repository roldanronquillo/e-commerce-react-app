import { useParams, useNavigate } from 'react-router-dom';
import {Container, Row, Col, Button, InputGroup, FormControl, Spinner} from 'react-bootstrap';
import { useEffect, useState, Fragment } from 'react';
import { Link } from 'react-router-dom';
// import UserContext from '../UserContext';

import Sidebar from '../components/Sidebar';
import ImageCarousel from '../components/ImageCarousel';
// import QuantitySelector from '../components/QuantitySelector';

import toastr from 'toastr';

import bread from '../images/device.jpg';

const admin = localStorage.getItem('admin');

export default function Product(){

	const {productId} = useParams();

	const [productName, setProductName] = useState("");
	const [price, setPrice] = useState("");
	const [description, setDescription] = useState("");
	const [quantity, setQuantity] = useState(1);
	const [cart, setCart] = useState("");
	const [stockCount, setStockCount] = useState(0);
	const [image, setImage] = useState([bread]);

	const [isLoading, setIsLoading] = useState(false);

	// const { user } = useContext(UserContext);
	const navigate = useNavigate();

	useEffect(()=>{

		// if(admin === "true"){
		// 	navigate(`../all-products`);
			
		// } else {
			fetchData();
		// }
	},[])

	const fetchData = () => {

		setIsLoading(true);

		fetch(`https://capstone2ecommerceapi.herokuapp.com/api/products/product/${productId}`)
		.then(result => result.json())
		.then(result => {

			setIsLoading(false);

			//assign values to useState
			setProductName(result.name);
			setPrice(result.price);
			setDescription(result.description);
			setStockCount(result.stockCount);
			
			if (result.image.length > 0 ){
				setImage(result.image)
			}

		});
	}

	//child to parent
	// const getQuantity = (qty) => {
	// 	console.log(`parent get quantity`)

	// 	setQuantity(qty);
		
	// 	console.log(`data from child`, qty);
	// 	console.log(`value parent quantity`, quantity);
	// } 

	const addQty = () => {
		if (stockCount > quantity ){
			setQuantity(quantity + 1);
		} else {
			toastr.error(`Cannot add more. This is the last stock for this item.`);
		}
	}

	const decQty = () => {
		if ( quantity > 1 ){
				setQuantity(quantity - 1);
		}
	}

	const addToCart = () => {

		if (stockCount < quantity ){
			toastr.warning(`Item count in cart exceeds current stocks. Please decrease.`);
		
		} else if ( quantity === `0`) {
			toastr.warning(`Please specify product quantity. Cannot add 0.`);
		} else if ( quantity === "") {
			toastr.warning(`Please specify product quantity. Cannot add 0.`);
		} else {
			let order = {
				productId: productId,
				productName: productName,
				productQty: quantity,
				productPrice: price,
				subTotal: price * quantity
			};

			if ( localStorage.getItem('orders') !== null){
				let cart = JSON.parse(localStorage.getItem('orders'));

				if (cart.some(item=>item.productId === order.productId)){
					toastr.error(`Product already in cart.`)
				} else {
					cart.push(order);
					localStorage.setItem('orders', JSON.stringify(cart));
					toastr.success(`Order added to cart`);
				}

			//add to local storage if not exists
			} else {
				
				let orderArr = [];
				orderArr.push(order);
				localStorage.setItem('orders', JSON.stringify(orderArr));
				toastr.success('Order added to cart.');
				// alert('Order added to cart.');
			}

			setCart(order);
		}

	}

	return(
		<Container fluid className="welcome m-0 p-0" style={{overflow:'auto'}}>
			<Row>
				<Col md={3} className="d-none d-sm-table-cell">
					<Sidebar indicator={`cart`} cart={cart} />
				</Col>
				<Col md={9} className="p-5">
					<div style={{height:80}}/>
					<Row className="blackOpaque py-5">
						<Col>
							<Row className="mx-auto">
								<Col md={5} className="mx-0 fadeInElements" >
									{/*<ImageCarousel/>*/}
									<img src={image[0]} alt="product image" className="img-fluid"/>
								</Col>
								<Col md={7} className="m-0 p-0 my-auto">
									<Row>
										<Col xs={12} className="fadeInElements">
											{
												isLoading === false? 
												<div>
													<h1>{productName}</h1>
													<h5>&#8369;{(Number(price)).toFixed(2)}</h5>
													<p>{description}</p>
												</div>
												: 
												<div className="mx-auto my-5">
													<Spinner
												    	as="span"
												    	animation="border"
												    	size="sm"
												    	role="status"
												    	aria-hidden="true"
												   		variant="light"
													/><span className="loading"> Loading details</span></div>
											}
										</Col>
										<Col xs={12} className="fadeInElements">
											<center>
												{/*add to cart component*/}
												{/*<QuantitySelector getQuantity={getQuantity}/>*/}
												{
													admin === "false" ?
													<Fragment>
														<InputGroup className="mb-3 cartQty" style={{width:130}}>
														  <Button variant="light" onClick={()=>decQty()}>-</Button>
														  <FormControl
														    className="login cartNumberField"
														    type="number"
														    value={quantity}
														    onChange={(e)=> setQuantity(e.target.value)}
														  />
														  <center><Button variant="light" onClick={()=>addQty()}>+</Button></center>
														</InputGroup>
														<Button variant="danger" className="btn btn-danger m-2" onClick={()=>addToCart()}>Add to cart</Button>
													</Fragment>
													: <Fragment></Fragment>
												}
												<Link to={`/products`} className="btn tableBtn m-2">Back to products</Link>
											</center>
										</Col>
									</Row>
								</Col>
							</Row>
						</Col>
					</Row>
				</Col>
			</Row>
		</Container>
	)
}