import {useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Container, Row, Form, Col, Button, DropdownButton, Dropdown, InputGroup, Spinner } from 'react-bootstrap';
import defaultImg from './../images/default.png';
// import ImageCarousel from '../components/ImageCarousel';

import toastr from 'toastr';

import $ from 'jquery';

export default function NewProduct(){
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [category, setCategory] = useState(`Select category`);
	const [stocks, setStocks] = useState(0);
	// const [isActive, setIsActive] = useState("");
	const [imageSource, setImageSource] = useState(defaultImg);
	const [image, setImage] = useState([]);

	const [isLoading, setIsLoading] = useState(false);
	const [isProcessing, setIsProcessing] = useState(false);

	const navigate = useNavigate();

	if (localStorage.getItem('admin') !== "true"){
		navigate(`../products`);
	}


	const addProduct = (e) =>{
		e.preventDefault();

		setIsProcessing(true);

		fetch(`https://capstone2ecommerceapi.herokuapp.com/api/products/new-product`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				category: category,
				stockCount: stocks
				,image: image 
			})
		})
		.then(result => result.json())
		.then(result => {

			setIsProcessing(false);

			if (result === true){
				toastr.success(`New product created.`);
				navigate(`../all-products`);
			} else {
				toastr.error(result.message);
			}
		})
	}

	const handleDDown = (e) => {
		setCategory(e);
	}

	const uploadPicture = (e) => {

		setIsLoading(true);

		//convert image to reader format for upload to database
		let fReader = new FileReader();
		let tempImgArray = [];

		fReader.readAsDataURL(e.target.files[0]);
		
		fReader.onload = () => {
			setImageSource(fReader.result);
			tempImgArray.push(fReader.result);
			setIsLoading(false);
		}

		setImage(tempImgArray);
	}

	return (
		<Container fluid className="adminPage min-vh-100 mx-auto">
			<Row className="fluid">
				<Col className="mx-auto my-auto">
					<Form onSubmit={(e) => addProduct(e)} className="fadeInElements">
						<Row className="my-auto">
							<Col md={5} className="tableBackground my-auto p-3 fluid ml-auto" style={{height:'433px'}}>
							{/*<ImageCarousel/>*/}
								<center>
									<h4>Upload Picture</h4>
									{
										isLoading === true ? 
										<div>
											<Spinner
											    as="span"
											    animation="border"
											    size="sm"
											    role="status"
											    aria-hidden="true"
											   	variant="light"
											/>
											<h3 className="mt-5">Uploading picture</h3>
										</div>
										:<a href="#">
											<img 
												src={imageSource} 
												alt="new product image" 
												className="img-fluid uploadImg"
												onClick={()=>$("#uploadImage").click()}
											/>
										</a>
									}
									<input 
										type="file" 
										id="uploadImage" 
										onChange={uploadPicture} 
										accept="image/*" 
										hidden
									/>
								</center>
							</Col>
							<Col className="tableBackground my-auto p-3 fadeInElements mr-auto" md={3}>
								<h3 className="loginLabel2 fadeInElements">NEW PRODUCT</h3>
								{/*<Form onSubmit={(e) => addProduct(e)} className="fadeInElements">*/}
							  	<Form.Group className="mb-3">
							  		<Form.Control 
							  			type="text" 
							  			className="login2" 
							  			value={name}
							  			placeholder="Product name" 
							  			onChange={(e)=> setName(e.target.value)}
							  			required
							  		/>
							  	</Form.Group>

							  	<Form.Group className="mb-3">
							  		<InputGroup>
							  			<InputGroup.Text className="igText">&#8369;</InputGroup.Text>
							    		<Form.Control 
							    			type="number" 
							    			className="login2" 
							    			value={price}
							    			placeholder="Price"
							    			onChange={(e)=> setPrice(e.target.value)}
							    			required
							    		/>
							    	</InputGroup>
							    </Form.Group>
							  

							  	<Form.Group className="mb-3">
							    	<Form.Control 
							    		as="textarea" 
							    		className="login2" 
							    		value={description}
							    		placeholder="Description"
							    		onChange={(e)=> setDescription(e.target.value)}
							    		row="3"
							    		required
							    	/> 
							  	</Form.Group>

							  	<Form.Group className="mb-3">
									<DropdownButton 
										id="prodDropdown"
										title={category} 
										className="btn-block mainButton2"
										onSelect={handleDDown} >
									    <Dropdown.Item eventKey="electronic_appliance" className="dropdownBg">Electronic_Appliance</Dropdown.Item>
									    <Dropdown.Item eventKey="gadget" className="dropdownBg">Gadget</Dropdown.Item>
									    <Dropdown.Item eventKey="gizmo" className="dropdownBg">Gizmo</Dropdown.Item>
									    <Dropdown.Item eventKey="mobile_devices" className="dropdownBg">Mobile_Device</Dropdown.Item>
									    <Dropdown.Item eventKey="security_device" className="dropdownBg">Security_Device</Dropdown.Item>
									</DropdownButton>
							  </Form.Group>

							  <Form.Group className="mb-3">
							  	<InputGroup>
							  		<InputGroup.Text className="igText">Stocks</InputGroup.Text>
							    <Form.Control 
							    	type="number" 
							    	className="login2" 
							    	value={stocks}
							    	placeholder="Stocks"
							    	onChange={(e)=> setStocks(e.target.value)}
							    	required
							    />
							    </InputGroup>
							  </Form.Group>

							  <center>
							  	<Button variant="outline-light" type="submit" className="mainButton2">
							    {
							    	isProcessing === false? `Submit` :
							    	<div>
							    		<Spinner
							    	    	as="span"
							    	    	animation="border"
							    	    	size="sm"
							    	    	role="status"
							    	    	aria-hidden="true"
							    	   		variant="light"
							    		/> Processing</div>
							    }
							  	</Button>
							  	<Button 
							  		variant="outline-light" 
							  		type="submit" 
							  		className="mainButton2 m-2" 
							  		onClick={()=>navigate(`../all-products`)}>
							  	  Cancel
							  	</Button>
							  </center>
							{/*</Form>*/}
							</Col>
						</Row>
					</Form>	
				</Col>
			</Row>	
		</Container>
	)
}