import { Container, Row, Col, Spinner } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

import ProductCard from '../components/ProductCard';
import Sidebar from '../components/Sidebar';
import UserContext from '../UserContext';

const admin = localStorage.getItem('admin');
const firstName = localStorage.getItem('firstName');

export default function Products(){

	const [products, setProducts] = useState([]);

	const [isLoading, setIsLoading] = useState(false);

	const {category} = useParams();
	const { state, dispatch } = useContext(UserContext);
	const navigate = useNavigate();

	// const screenSize = window.innerWidth;

	// -------------------------------------------------------------------------------------------
	//retrieve products upon page load
	useEffect(()=>{

		if (admin === "true"){
			dispatch({type:"ADMIN", name: firstName, admin: true});
		} else if (admin === "false") {
			dispatch({type:"USER", name: firstName, admin: false});
		}

		if (state.admin === true || admin==="true"){
			navigate(`../all-products`);
		} else{
			fetchData();
		} 

		// if (screenSize <= 1440){
		// 	setItemsPerPage(6);
		// } else if (screenSize > 1440 && screenSize < 2560 ){
		// 	setItemsPerPage(8);
		// } else {
		// 	setItemsPerPage(16);
		// }



	},[category])

	// --------------------------------------------------------------------------------------------
	const fetchData = () => {

		setIsLoading(true);

		let fetchAPI = getAPI();

		fetch(fetchAPI).then(result=>result.json()).then(result => {

			setProducts(
				result.map(product=>{
					return <ProductCard key={product._id} productProp={product}/>
				})
			)

			setIsLoading(false);
		})
	}

	const getTitle = () => {
		switch(category){
			case `electronic_appliance`: return `Electronic_Appliance`;
							// break;
			case `gadget`: return `Gadget`;
							// break;
			case `gizmo`: return `Gizmo`;
							// break;
			case `mobile_devices`: return `Mobile_Device`;
							// break;
			case `security_device`: return `Security_Device`;
							// break;
			case `featured`: return `RECOMMENDED`;
							// break;
			case `best-seller`: return `BEST SELLERS`;
							// break;
			default: return `RECOMMENDED`;
		}
	}

	const getAPI = () => {
		switch(category){
			case `featured`: return `https://capstone2ecommerceapi.herokuapp.com/api/products/featured`;
			case `best-seller`: return `https://capstone2ecommerceapi.herokuapp.com/api/products/best-seller/8`;
			case `electronic_appliance`: return `https://capstone2ecommerceapi.herokuapp.com/api/products/category/Electronic_Appliance`;
			case `gadget`: return `https://capstone2ecommerceapi.herokuapp.com/api/products/category/Gadget`;
			case `gizmo`: return `https://capstone2ecommerceapi.herokuapp.com/api/products/category/Gizmo`;
			case `mobile_devices`: return `https://capstone2ecommerceapi.herokuapp.com/api/products/category/Mobile_Device`;
			case `security_device`: return `https://capstone2ecommerceapi.herokuapp.com/api/products/category/Security_Device`;
			default: return `https://capstone2ecommerceapi.herokuapp.com/api/products/featured`;
		}
	}


	//display page ----------------------------------------------------------------------------------
	return(
		<Container fluid className="welcome" style={{overflowY:'auto'}}>
			<Row>
				<Col md={2} className="p-0">
					<Sidebar indicator={`products`}/>
				</Col>

				<Col md={10} className="mx-auto">
					<div style={{height:50}}/>
					<h1 className="my-3">
						<span className="loginLabel productAnimate">
							{getTitle()}
						</span>
					</h1>
					<div className="d-flex flex-wrap" >
						{
							isLoading === false?
							//getDisplay()
							products
							: 					    
					    	<div className="mx-auto mt-5">
					    		<Spinner
					    	    	as="span"
					    	    	animation="border"
					    	    	size="sm"
					    	    	role="status"
					    	    	aria-hidden="true"
					    	   		variant="light"
					    		/><span className="loading"> Retrieving products</span></div>
						}
					</div>
				</Col>
			</Row>
		
		</Container>
	)

}