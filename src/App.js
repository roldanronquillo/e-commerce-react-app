//CSS Imports
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import 'toastr/toastr.scss';

//React Components
import { BrowserRouter, Routes, Route , } from 'react-router-dom';
import { useReducer } from 'react';

//Component Imports
import AppNavbar from './components/AppNavbar';

//Page Imports
import Welcome from './pages/Welcome';
import Login from './pages/Login';
import Register from './pages/Register';
import Error from './pages/Error';
import Products from './pages/Products';
import Product from './pages/Product';
import ProductList from './pages/ProductList';
import UpdateProduct from './pages/UpdateProduct';
import NewProduct from './pages/NewProduct';
import Cart from './pages/Cart';
import Order from './pages/Order';
import OrderHistory from './pages/OrderHistory';
import Logout from './pages/Logout';
import Profile from './pages/Profile';
import UserList from './pages/UserList';
import PaymentList from './pages/PaymentList';
import Payment from './pages/Payment';
import SearchProduct from './pages/SearchProduct';
import AdminSearch from './pages/AdminSearch';

//UserContext
import { UserProvider } from './UserContext';
import {initialState, reducer} from './UserReducer'

//toastr - customized alerts
import toastr from "toastr";

//toastr settings
toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-bottom-center",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

function App() {

  const [state, dispatch] = useReducer(reducer, initialState);

  return (
   <UserProvider value={{state, dispatch}}>
      <BrowserRouter>
        <AppNavbar/>

        <Routes>
          <Route path="/" element={<Welcome/>} />
          <Route path="/login" element={<Login/>} />
          <Route path="/register" element={<Register/>} />
          <Route path="/products/:category" element={<Products/>} />
          <Route path="/products/" element={<Products/>} />
          <Route path="/product/:productId" element={<Product/>} />
          <Route path="/all-products" element={<ProductList/>} />
          <Route path="/all-products/:category" element={<ProductList/>} />
          <Route path="/new-product" element={<NewProduct/>} />
          <Route path="/update-product/:productId" element={<UpdateProduct/>} />
          <Route path="/order-details/:orderId" element={<Order/>} />
          <Route path="/order-history" element={<OrderHistory/>} />
          <Route path="/cart" element={<Cart/>}/>
          <Route path="/logout" element={<Logout/>}/>
          <Route path="/profile" element={<Profile/>}/>
          <Route path="/profile/:userId" element={<Profile/>}/>
          <Route path="/all-users" element={<UserList/>} />
          <Route path="/all-payments" element={<PaymentList/>} />
          <Route path="/payment-details/:paymentId" element={<Payment/>}/>
          <Route path="/search-product" element={<SearchProduct/>} />
          <Route path="/search-all-products" element={<AdminSearch/>} />
          <Route path="*" element={<Error/>} />
        </Routes>
      </BrowserRouter>
      </UserProvider>

  );
}

export default App;
