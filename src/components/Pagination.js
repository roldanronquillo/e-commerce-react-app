//TUTORIAL: https://www.youtube.com/watch?v=IYCa1F-OWmk&ab_channel=TraversyMedia

import React from 'react';
import {Pagination} from 'react-bootstrap';

export default function Paginate({ itemsPerPage, totalItems, paginate}){
	const pageNumbers = [];

	for (let i = 1; i <= Math.ceil(totalItems/itemsPerPage); i++){
		pageNumbers.push(i);
	}

	return(
		// <nav>
		// 	<ul className="pagination">
		// 		{pageNumbers.map(number=>(
		// 			<li key={number} className="page-item">
		// 				<a href="!#" className="page-link">{number}</a>
		// 			</li>
		// 		))}
		// 	</ul>
		// </nav>
		<Pagination>
			{pageNumbers.map(number=>(
				<Pagination.Item key={number}>
					{number}
				</Pagination.Item>
			))}
		</Pagination>
	)
}