import {Navbar, Container, Nav, Button, NavDropdown} from 'react-bootstrap'
import {Fragment} from 'react';

import userLogo from '../images/user.png';
import cartLogo from '../images/cart.png';
import productLogo from '../images/product.png';
import homeLogo from '../images/home.png';
import ReactTooltip from "react-tooltip";

export default function AppNavbar(){

	const token = localStorage.getItem('token');
	const admin = localStorage.getItem('admin');

	 return(
	 	<Navbar expand="lg" className="m-0 p-0 navbar shadow" fixed="top">
	 		<Container fluid>
	 	    	<Navbar.Brand href="/" className="logo py-0 text-align-left">ROLLERS STORE</Navbar.Brand>
	 	    	<Navbar.Toggle aria-controls="basic-navbar-nav" />
	 	    	<Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
	 	      		<Nav className="me-auto my-0 mr-5 d-flex flex-row ">
	 	      		{
	 	      			token === null ? <Nav.Link href="/" className="navText"><img src={homeLogo} className="img-fluid navbarImg2" alt="Home"/></Nav.Link>
	 	      			: <Fragment></Fragment>
	 	      		}
	 	        		{
	 	        			admin === "false" ? 
	 	        			<Fragment>
	 	        				<Nav.Link href="../products" className="navText">
	 	        				<img src={cartLogo} alt="cart" className="navbarImg2 img-fluid" />
	 	        				   
							      </Nav.Link>
	 	        				<Nav.Link href="/cart" className="navText"><img src={productLogo} alt="product" className="navbarImg2 img-fluid"/>
	 	        				</Nav.Link> 
	 	        			</Fragment>
	 	        			: <Nav.Link href="../all-products" className="navText"><img src={cartLogo} alt="cart" className="navbarImg2 img-fluid" data-tip data-for="registerTip"/>
	 	        			     <ReactTooltip id="registerTip" place="bottom" effect="solid">
							        Clickhere! for Guest Users
							      </ReactTooltip>
							      </Nav.Link>
	 	        		}
	 	        		<NavDropdown title={<img src={userLogo} className="navbarImg img-fluid" alt="user"/>} id="basic-nav-dropdown" className="navText" >
	 	        		          
	 	          			
	 	          			{
	 	          				token === null 
	 	          				? <Fragment>
	 	          					<NavDropdown.Item href="../login" className="navText">Log-in</NavDropdown.Item>
	 	          					<NavDropdown.Item href="../register" className="navText">Register</NavDropdown.Item>
	 	          				</Fragment>
	 	          				: <Fragment>
	 	          					<Fragment></Fragment>: <NavDropdown.Item href="../profile" className="navText">Profile</NavDropdown.Item>
	 	          					<NavDropdown.Item href="../order-history" className="navText">Order History</NavDropdown.Item>
	 	          					<NavDropdown.Item href="../all-payments" className="navText">Payment History</NavDropdown.Item>
	 	          					<NavDropdown.Divider />
	 	          					<NavDropdown.Item href="../logout" className="navText">Log-out</NavDropdown.Item>
	 	          				</Fragment>
	 	          			}
	 	        		</NavDropdown>
	 	      		</Nav>
	 	    	</Navbar.Collapse>
	 	  </Container>
	 	</Navbar>
	)
}