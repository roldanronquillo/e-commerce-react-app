import {Carousel, Container } from 'react-bootstrap';
import { useState } from 'react';
import image1 from './../images/device.jpg';
import image2 from './../images/device2.jpg';
import image3 from './../images/device3.jpg';

export default function ImageCarousel() {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  return (
    <Container className="w-100 m-0 p-0">
      <Carousel activeIndex={index} onSelect={handleSelect} className="fadeInElements">
        <Carousel.Item interval={60000}>
          <img
            className="d-block img-fluid"
            // src="holder.js/800x400?text=First slide&bg=373940"
            src={image1}
            alt="First slide"
          />
        </Carousel.Item>
        <Carousel.Item interval={60000}>
          <img
            className="d-block img-fluid"
            // src="holder.js/800x400?text=Second slide&bg=282c34"
            src={image2}
            alt="Second slide"
          />
        </Carousel.Item>
        <Carousel.Item interval={60000}>
          <img
            className="d-block img-fluid"
            // src="holder.js/800x400?text=Third slide&bg=20232a"
            src={image3}
            alt="Third slide"
          />
        </Carousel.Item>
      </Carousel>
    </Container>
  );
}