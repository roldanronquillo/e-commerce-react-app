import { Container, Row, Col} from 'react-bootstrap';
import image from './../images/device.jpg';
import {Link} from 'react-router-dom';
import device from '../images/device.jpg';

export default function ProductCard({productProp}){

	let { _id, name, price, image } = productProp;

	if (image.length === 0){
		image = [device];
	}

	return (
			<Container className="productCard mx-auto my-2 d-flex p-0 productAnimate" style={{overflow:'hidden'}}>
				<Link to={`/product/${_id}`}>
				<Row>
					<Col md={6}>
						<img src={image[0]} className="img-fluid" alt="product"/>
					</Col>
					<Col md={6} className="p-3">
						<h5 className="productText">{name}</h5>
						<p className="productText">&#8369;{price.toFixed(2)}</p>
					</Col>
				</Row>
				</Link>
			</Container>
	)
}